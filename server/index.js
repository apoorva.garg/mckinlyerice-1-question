const express = require('express')
const mongoose = require('mongoose')
const bodyParser = require('body-parser')
// const config = require('./config/key')
const cors = require('cors');

const Chat = require('./models/chat');


const connect = mongoose.connect('mongodb://localhost/mckinley-rice-1', { useNewUrlParser: true })
    .then(() => console.log("yes"))
    .catch(err => console.log(err))

const app = express();
const port = process.env.PORT || 5000
const server = app.listen(port)
const io = require('socket.io').listen(server)
const authRoute = require('./routes/auth')
const chatRoute = require('./routes/chats')

app.use(bodyParser.json())
app.use(bodyParser.urlencoded({ extended: true }))


app.use(cors());
app.use('/auth', authRoute)
app.use('/chats', chatRoute)


app.use((req, res, next) => {
    res.setHeader('Access-Control-Allow-Origin', 'http://localhost:3000');
    res.setHeader(
        'Access-Control-Allow-Methods',
        'OPTIONS, GET, POST, PUT'
    );
    res.setHeader('Access-Control-Allow-Headers', 'Content-Type, Authorization');
    res.setHeader('Access-Control-Allow-Credentials', 'true');
    next();
})

io.on('connection', (socket) => {
    socket.on("Input Chat Message", msg => {

        connect.then(db => {
            try {
                let chat = new Chat({ message: msg.chatMessage, sender: msg.userId })

                chat.save((err, doc) => {
                    if (err) return res.json({ success: false, err })

                    Chat.find({ "_id": doc._id })
                        .populate("sender")
                        .exec((err, doc) => {

                            return io.emit("Output Chat Message", doc);
                        })
                })
            } catch (error) {
                console.error(error);
            }
        })
    })
})


app.use((error, req, res, next) => {
    const status = error.statusCode || 500;
    const message = error.message;
    const data = error.data;
    res.status(status).json({ message: message, data: data });
})