const bcrypt = require('bcryptjs');
const User = require('../models/user')

exports.register = async (req, res, next) => {
    try {
        const user = new User(req.body)
        const result = await user.save()
        res.status(201).json({ success: true, userId: result._id })
    } catch (error) {
        if (!error.statusCode) {
            error.statusCode = 500;
        }
        next(error)
    }
}

exports.login = async (req, res, next) => {
    try {
        const user = await User.findOne({ email: req.body.email })
        if (!user) {
            const error = new Error('Email not found')
            error.statusCode = 401;
            throw error
        }
        const isEqual = await user.comparepassword(req.body.password);
        if (!isEqual) {
            const error = new Error('Password not Matched')
            error.statusCode = 401;
            throw error
        }

        const userToken = await user.generateToken();
        if (!userToken) {
            const error = new Error('Password not Matched')
            error.statusCode = 401;
            throw error
        }

        res.status(200).json({ success: true, token: userToken.token, name: user.name, userId: user._id })
    } catch (error) {
        if (!error.statusCode) {
            error.statusCode = 500
        }
        next(error);
    }
}