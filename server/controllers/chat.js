const  Chat  = require("../models/chat");

exports.getChats = async (req, res, next) => {
    try {
        const chat = Chat.find()
            .populate("sender")
            .exec((err, chats) => {
                if (err) {
                    const error = new Error('Something went wrong')
                    error.statusCode = 400;
                    throw error
                }
                res.status(200).json({chats: chats})
            })
    } catch (error) {
        if (!error.statusCode) {
            error.statusCode = 500;
        }
        next(error)
    }
}

