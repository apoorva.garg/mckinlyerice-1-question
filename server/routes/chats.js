const express = require('express')

const chatController = require('../controllers/chat')

const router = express.Router();

router.get('/getChats', chatController.getChats)


module.exports = router