import React from "react";
import moment from 'moment';
import { Comment, Tooltip } from 'antd';

const ChatCard = (props) => {
    return (
        <div style={{ width: '100%' }}>
            <Comment
                author={props.sender.name}
                content={
                    <p style={{ fontSize: '18px', fontWeight: '500' }}>
                        {props.message}
                    </p>
                }
                datetime={
                    <Tooltip title={moment().format('YYYY-MM-DD HH:mm:ss')}>
                        <span>{moment().fromNow()}</span>
                    </Tooltip>
                }
            />
        </div>
    )
}

export default ChatCard;