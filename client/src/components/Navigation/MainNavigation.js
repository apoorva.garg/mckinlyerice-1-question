import React from 'react';
import { NavLink } from 'react-router-dom';

import AuthContext from '../../context/auth-context';
import './MainNavigation.css';

const mainNavigation = (props) => (
    <AuthContext.Consumer>
        {(context) => {
            return (
                <header className="main-navigation">
                    <div className="main-navigation-title">
                        <h1>Chat</h1>
                    </div>
                    <nav className="main-navigation-items">
                        <ul>
                            {!context.token && <li><NavLink to="/login">Authenticate</NavLink></li>}
                            {context.token && (<React.Fragment>
                                <li><NavLink to="/chat">Chat</NavLink></li>
                                <li><button onClick={context.logout}>Logout</button></li>
                            </React.Fragment>)}
                        </ul>
                    </nav>
                </header>
            )
        }}
    </AuthContext.Consumer>
);

export default mainNavigation