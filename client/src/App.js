import React, { Component } from 'react';
import { BrowserRouter, Route, Redirect, Switch } from 'react-router-dom'
import './App.css';


import AuthPage from './pages/Auth';
import ChatPage from './pages/Chat';
import MainNavigation from './components/Navigation/MainNavigation';
import AuthContext from './context/auth-context';


class App extends Component {

  state = {
    token: null,
    userId: null
  }

  login = (token, userId, userName) => {
    this.setState({ token: token, userId: userId, userName: userName })
  }

  logout = () => {
    this.setState({ token: null, userId: null, userName: null })
  }

  render() {
    return (
      <BrowserRouter>
        <React.Fragment>
          <AuthContext.Provider value={{ token: this.state.token, userId: this.state.userId, login: this.login, logout: this.logout, userName: this.state.userName }} >
            <MainNavigation />
            <main className="main-content">
              <Switch>
                {!this.state.token && <Redirect from="/" to="/login" exact />}
                {this.state.token && <Redirect from="/login" to="/chat" exact />}
                {!this.state.token && <Route path="/login" component={AuthPage} />}
                {this.state.token && <Route path="/chat" component={ChatPage} />}
              </Switch>
            </main>
          </AuthContext.Provider>
        </React.Fragment>
      </BrowserRouter >
    );
  }
}

export default App;