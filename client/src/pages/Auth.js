import React, { Component } from 'react';
import './Auth.css'

import Input from '../components/Input/Input';
import Button from '../components/Button/Button';
import AuthContext from '../context/auth-context';


class AuthPage extends Component {

    state = {
        isLogin: true
    };

    static contextType = AuthContext;

    constructor(props) {
        super(props);
        this.emailEl = React.createRef();
        this.passwordEl = React.createRef();
        this.confirmpasswordEl = React.createRef();
        this.nameEl = React.createRef();
        this.lastnameEl = React.createRef();
    }

    submitHandler = (event) => {
        event.preventDefault()
        const email = this.emailEl.current.value;
        const password = this.passwordEl.current.value;

        if (email.trim().length === 0 || password.trim().length === 0) {
            return;
        }

        let requestbody = {
            email: email,
            password: password
        }

        if (!this.state.isLogin) {
            const name = this.nameEl.current.value;
            const lastname = this.lastnameEl.current.value;
            const confirmpassword = this.confirmpasswordEl.current.value;
            requestbody = {
                email: email,
                password: password,
                name: name,
                lastname: lastname,
                confirmpassword: confirmpassword
            }
        }

        fetch(`http://localhost:5000/auth/${this.state.isLogin ? 'login' : 'register'}`, {
            method: 'POST',
            body: JSON.stringify(requestbody),
            headers: {
                'Content-Type': 'application/json'
            }
        }).then((res) => {
            if (res.status !== 200 && res.status !== 201) {
                res.json().then(err => console.error(err.message))
                throw new Error('Failed!!')
            }
            return res.json()
        })
            .then((resData) => {
                if (resData.success && this.state.isLogin) {
                    this.context.login(resData.token, resData.userId, resData.name)
                }
                else {
                    this.setState({ isLogin: true })
                }
            })
            .catch(err => {
                console.log("1", err);
            })
    }

    switchModeHandler = () => {
        this.setState(prevState => {
            return { isLogin: !prevState.isLogin }
        })
    }

    render() {
        let signUpName = ''
        if (!this.state.isLogin) {
            signUpName = (
                <div>
                    <Input type="text" id="name" label="Name" placeholder="Enter Name" refel={this.nameEl} />
                    <Input type="text" id="lastname" label="Last Name" placeholder="Enter Last Name" refel={this.lastnameEl} />
                </div>
            );
        }
        return (
            <React.Fragment>
                <form className="auth-form">
                    <div>
                        {signUpName}
                        <Input type="email" id="email" label="Email" placeholder="Enter Email" refel={this.emailEl} />
                        <Input type="password" id="password" placeholder="Enter Password" label="Password" refel={this.passwordEl} />
                        {!this.state.isLogin && (
                            <Input type="password" id="confirmpassword" placeholder="Enter COnfirm Password" label="Confirm Password" refel={this.confirmpasswordEl} />
                        )}
                    </div>
                    <div className="button-center">
                        <Button clicked={this.submitHandler}>Submit</Button>
                        <Button clicked={this.switchModeHandler}>Want to {this.state.isLogin ? 'SignUp' : 'SignIn'} ?</Button>
                    </div>
                </form>
            </React.Fragment>
        );
    }
}

export default AuthPage;