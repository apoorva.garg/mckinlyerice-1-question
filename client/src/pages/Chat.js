import React, { Component } from 'react';
import { Form, Row, Col } from 'antd';
import io from "socket.io-client";
import moment from "moment";

import './Chat.css';

import Button from '../components/Button/Button'
import Input from '../components/Input/Input'
import AuthContext from '../context/auth-context';
import ChatCard from '../components/ChatCard/ChatCard'

class ChatPage extends Component {

    state = {
        chats: []
    }

    constructor(props) {
        super(props);
        this.messageRef = React.createRef();
    }

    static contextType = AuthContext

    componentDidMount() {
        let server = "http://localhost:5000";

        this.getChats()

        this.socket = io(server);

        this.socket.on("Output Chat Message", messageFromBackEnd => {
            const oldChats = this.state.chats;
            const newChats = [...oldChats, messageFromBackEnd[0]]
            this.setState({ chats: newChats })
        })
    }

    componentDidUpdate() {
        this.messagesEnd.scrollIntoView({ behavior: 'smooth' });
    }

    getChats() {
        fetch(`http://localhost:5000/chats/getChats`, {
            method: 'GET',
            headers: {
                'Content-Type': 'application/json'
            }
        }).then(res => {
            if (res.status !== 200 && res.status !== 201) {
                res.json().then(err => console.error(err.message))
                throw new Error(res)
            }
            return res.json()
        })
            .then((resData) => {
                this.setState({ chats: resData.chats })
            })
            .catch(err => {
                console.log(err)
            })
    }

    submitChatMessage = (id, name) => {
        let chatMessage = this.messageRef.current.value
        let userId = id
        let userName = name;
        let nowTime = moment();


        this.socket.emit("Input Chat Message", {
            chatMessage,
            userId,
            userName,
            nowTime
        });
        this.messageRef.current.value = ''
    }

    render() {
        return (
            <AuthContext.Consumer>
                {(context) => {
                    return (
                        <React.Fragment>
                            <div>
                                <p style={{ fontSize: '2rem', textAlign: 'center' }}> Real Time Chat</p>
                            </div>

                            <div style={{ maxWidth: '800px', margin: '0 auto' }}>
                                <div className="infinite-container" style={{ height: '650px', overflowY: 'scroll' }}>
                                    {this.state.chats && this.state.chats.map(chat => (
                                        <ChatCard key={chat._id} {...chat} />
                                    ))}
                                    <div
                                        ref={el => {
                                            this.messagesEnd = el;
                                        }}
                                        style={{ float: "left", clear: "both" }}
                                    />
                                </div>
                                <Row>
                                    <Form layout="inline">
                                        <Col span={20}>
                                            <Input
                                                id="message"
                                                placeholder="Let's start talking"
                                                type="text"
                                                refel={this.messageRef}
                                            />
                                        </Col>
                                        <Col span={2}>

                                        </Col>

                                        <Col span={4} style={{marginTop: '15px'}}>
                                            <Button type="submit" clicked={() => this.submitChatMessage(context.userId, context.userName)}>Send</Button>
                                        </Col>
                                    </Form>
                                </Row>
                            </div>
                        </React.Fragment>
                    )
                }

                }
            </AuthContext.Consumer>
        )
    }
}


export default ChatPage;